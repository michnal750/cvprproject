import tensorflow as tf
from tkinter import *
from tkinter import filedialog, ttk
from PIL import ImageTk, Image
from tensorflow.keras.applications.resnet50 import preprocess_input
from tensorflow.keras.preprocessing import image
import numpy as np


def openfilename():
    # open file dialog box to select image
    # The dialogue box has a title "Open"
    global filename
    filename = filedialog.askopenfilename(initialdir="/", title="Select a image file",
                                          filetypes=(("png files", "*.png"), ("jpg files", "*.jpg")))
    return filename


def open_img():
    # Select the Imagename  from a folder
    x = openfilename()

    # opens the image
    img = Image.open(x)

    # resize the image and apply a high-quality down sampling filter
    img = img.resize((250, 250), Image.ANTIALIAS)

    # PhotoImage class is used to add image to widgets, icons etc
    img = ImageTk.PhotoImage(img)

    # create a label
    panel = Label(root, image=img)

    # set the image as img
    panel.image = img
    panel.grid(row=3)


def classify():
    if not filename:
        return
    CATEGORIES = ['Pidgeotto', 'Pidgey', 'Pikachu', 'Pinsir', 'Poliwag', 'Poliwrath', 'Ponyta', 'Primeape', 'Psyduck',
                  'Raichu']
    img = image.load_img(filename, target_size=(224, 224))
    img_array = image.img_to_array(img)

    img_batch = np.expand_dims(img_array, axis=0)

    img_preprocessed = preprocess_input(img_batch)

    model = tf.keras.models.load_model('VGGModel2.h5')
    prediction = model.predict(img_preprocessed)
    sampleArray = np.array(prediction)
    convertedArray = np.round(sampleArray, 5)
    index = np.argmax(prediction[0])
    print(CATEGORIES[index])
    prediction_label = ttk.Label(
        root,
        text=CATEGORIES[index]
    ).grid(row=4, columnspan=4)
    print(convertedArray)


root = Tk()
root.title('Image classifier')
root.resizable(False, False)
root.geometry('500x500')

open_button = ttk.Button(
    root,
    text='Open a File',
    command=lambda: open_img(),
    width=50
).grid(row=1, columnspan=1, pady=10, padx=90)

classify_button = ttk.Button(
    root,
    text='Classify',
    command=lambda: classify(),
    width=50
).grid(row=2, columnspan=1, pady=10)

panel = Label(root)

# run the application
root.mainloop()
